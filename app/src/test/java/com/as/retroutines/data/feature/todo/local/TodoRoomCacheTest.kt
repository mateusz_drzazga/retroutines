package com.`as`.retroutines.data.feature.todo.local

import com.google.gson.Gson
import com.google.gson.JsonParseException
import com.google.gson.JsonSyntaxException
import com.google.gson.reflect.TypeToken
import org.junit.Assert
import org.junit.Test

class TodoRoomCacheTest {
    val json = """[
  {
    "userId": 1,
    "id": 1,
    "title": "delectus aut autem",
    "completed": false
  },
  {
    "userId": 1,
    "id": 2,
    "title": "quis ut nam facilis et officia qui",
    "completed": false
  }]""".trimIndent()

    val invalidJson = """[
  {
    "userId": "s",
    "title": "delectus aut autem",
    "completed": false
  },
  {
    "userId": 1,
    "id": 2,
    "title": "quis ut nam facilis et officia qui",
    "completed": false
  }]""".trimIndent()

    val gson = Gson()
    val token = TypeToken.getParameterized(ArrayList::class.java, TodoEntity::class.java)

    @Test
    fun getTodos() {
        val parsed = gson.fromJson<List<TodoEntity>>(json, token.type)
        Assert.assertEquals(2, parsed.size)
    }

    @Test
    fun getTodosNull() {
        val nullString: String? = null
        val parsed = gson.fromJson<List<TodoEntity>>(nullString, token.type)

        Assert.assertEquals(null, parsed)
    }

    @Test(expected = JsonSyntaxException::class)
    fun getTodosSyntaxException() {
        val parsed = gson.fromJson<List<TodoEntity>>("{}", token.type)
        Assert.assertEquals(null, parsed)
    }

    @Test(expected = JsonParseException::class)
    fun getTodosParseException() {
        val parsed = gson.fromJson<List<TodoEntity>>(invalidJson, token.type)
    }
}