package com.`as`.retroutines.data.feature.common

import org.junit.Assert.*
import org.junit.Test

class ResultTest {


    @Test
    fun wrapCatching() {
        val result = com.`as`.retroutines.data.feature.common.wrapCatching {
            throw NullPointerException()
        }

        assertTrue(result is Result.Failure)
    }
}