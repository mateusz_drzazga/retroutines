package com.`as`.retroutines.domain.feature.users

class UserNotFoundException(val userId: Int) : Exception()