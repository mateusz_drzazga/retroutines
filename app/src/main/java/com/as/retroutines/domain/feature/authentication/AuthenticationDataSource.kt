package com.`as`.retroutines.domain.feature.authentication

import com.`as`.retroutines.data.feature.authentication.remote.Credentials
import com.`as`.retroutines.data.feature.authentication.remote.Token
import com.`as`.retroutines.data.feature.common.Result

interface AuthenticationDataSource {
    suspend fun authenticate(credentials: Credentials): Result<Token>
}