package com.`as`.retroutines.domain.feature.todo

data class Todo(val userId: Int, val id: Int, val title: String, val completed: Boolean)
