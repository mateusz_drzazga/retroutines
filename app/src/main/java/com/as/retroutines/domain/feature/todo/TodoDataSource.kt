package com.`as`.retroutines.domain.feature.todo

import com.`as`.retroutines.data.feature.common.Result
import com.`as`.retroutines.domain.feature.todo.Todo

interface TodoDataSource {
    suspend fun getTodos(): Result<List<Todo>>
}