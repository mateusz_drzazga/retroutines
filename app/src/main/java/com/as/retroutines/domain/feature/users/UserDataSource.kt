package com.`as`.retroutines.domain.feature.users

import com.`as`.retroutines.data.feature.common.Result

interface UserDataSource {
    suspend fun getUser(id: Int): Result<User>

    suspend fun deleteUser(user: User): Result<Boolean>
}