package com.`as`.retroutines.data.feature.users

import com.`as`.retroutines.data.feature.common.Result
import com.`as`.retroutines.data.feature.users.local.UserEntity

interface UserStore {

    suspend fun getUser(id: Int): Result<UserEntity>

    suspend fun deleteUser(user: UserEntity): Result<Boolean>
}