package com.`as`.retroutines.data.feature.todo.remote

import android.util.Log
import com.`as`.retroutines.data.MyService
import com.`as`.retroutines.data.feature.common.Result
import com.`as`.retroutines.data.feature.common.asFailure
import com.`as`.retroutines.data.feature.common.asSuccess
import com.`as`.retroutines.data.feature.todo.TodoStore
import com.`as`.retroutines.data.feature.todo.local.TodoCache
import com.`as`.retroutines.data.feature.todo.local.TodoEntity
import com.`as`.retroutines.data.feature.todo.toEntity
import java.net.UnknownHostException
import javax.inject.Inject

class TodoRemoteStore @Inject constructor(
        private val service: MyService,
        private val cache: TodoCache) : TodoStore {

    override suspend fun getTodos(): Result<List<TodoEntity>> {
        try {
            val response = service.getTodosInCoroutine()
            Log.v("STORE", "Response successful: ${response.isSuccessful}")

            when (response.isSuccessful) {
                true -> {
                    val todos = response.body()?.mapNotNull { it.toEntity() }
                    return if (todos != null) {
                        cache.saveTodos(todos)
                        todos.asSuccess()
                    } else {
                        Result.Failure(NullPointerException())
                    }
                }
                false -> {
                    return Result.Failure(NullPointerException())
                }
            }
        } catch (e: Throwable) {
            if (e is UnknownHostException) {
                Log.v("STORE", "UnknownHostException")
                return e.asFailure()
            } else {
                Log.v("STORE", "Unhandled exception: $e")
                throw e
            }
        }
    }
}