package com.`as`.retroutines.data.feature.todo

import com.`as`.retroutines.data.feature.common.Result
import com.`as`.retroutines.data.feature.todo.local.TodoEntity

interface TodoStore {
    suspend fun getTodos(): Result<List<TodoEntity>>
}