package com.`as`.retroutines.data.feature.authentication.remote

import com.`as`.retroutines.data.MyService
import com.`as`.retroutines.data.feature.authentication.AuthenticationStore
import com.`as`.retroutines.data.feature.common.*
import javax.inject.Inject

class AuthenticationRemoteStore @Inject constructor(private val service: MyService) : AuthenticationStore {

    override suspend fun authenticate(credentials: Credentials): Result<Token> {
        return catchNetworkException {
            val response = service.authenticate(credentials)
            return when (response.isSuccessful) {
                true -> response.body()?.asSuccess() ?: NullPointerException().asFailure()
                false -> FormHttpException(response).asFailure()
            }
        }
    }
}