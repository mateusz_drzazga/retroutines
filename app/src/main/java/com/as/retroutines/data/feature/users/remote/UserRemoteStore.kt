package com.`as`.retroutines.data.feature.users.remote

import android.util.Log
import com.`as`.retroutines.data.MyService
import com.`as`.retroutines.data.feature.common.*
import com.`as`.retroutines.data.feature.users.UserStore
import com.`as`.retroutines.data.feature.users.local.UserCache
import com.`as`.retroutines.data.feature.users.local.UserEntity
import com.`as`.retroutines.data.feature.users.toEntity
import com.`as`.retroutines.domain.feature.users.UserNotFoundException
import java.net.HttpURLConnection
import javax.inject.Inject

class UserRemoteStore @Inject constructor(private val service: MyService, private val cache: UserCache) : UserStore {
    private val TAG = "UserRemoteStore"
    override suspend fun getUser(id: Int): Result<UserEntity> {
        return catchNetworkException {
            val response = service.getUser(id)
            if (response.isSuccessful) {
                val userResponse = response.body()
                val entry = userResponse?.data
                if (entry != null) {
                    val entity = entry.toEntity()
                    cache.saveUser(entity)
                    return entity.asSuccess()
                } else {
                    return NullPointerException().asFailure()
                }
            } else {
                if (response.code() == HttpURLConnection.HTTP_NOT_FOUND) {
                    return Result.Failure(UserNotFoundException(id))
                }
                return createResponseFailure(response)
            }
        }
    }

    override suspend fun deleteUser(user: UserEntity): Result<Boolean> {
        return catchNetworkException {
            val response = service.deleteUser(user.id)
            return@catchNetworkException if (response.isSuccessful) {
                val cacheResult = cache.deleteUser(user)
                Log.v(TAG, "Deleted from cache: $cacheResult")
                Result.Success(true)
            } else {
                createResponseFailure<Boolean>(response)
            }
        }
    }
}