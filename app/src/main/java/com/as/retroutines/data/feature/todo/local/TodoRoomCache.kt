package com.`as`.retroutines.data.feature.todo.local

import android.content.SharedPreferences
import android.util.Log
import androidx.core.content.edit
import com.`as`.retroutines.data.feature.common.Result
import com.`as`.retroutines.data.feature.common.asFailure
import com.`as`.retroutines.data.feature.common.asSuccess
import com.google.gson.Gson
import com.google.gson.JsonParseException
import com.google.gson.reflect.TypeToken
import kotlinx.coroutines.delay
import java.security.KeyStoreException
import javax.inject.Inject

class TodoRoomCache @Inject constructor(private val prefs: SharedPreferences, private val gson: Gson) : TodoCache {
    private val TAG = "TodoRoomCache"
    override suspend fun saveTodos(todos: List<TodoEntity>) {
        delay(1000)
        if (prefs.contains(KEY)) throw KeyStoreException("Can't save, key already exists.")
        prefs.edit(true) { putString(KEY, gson.toJson(todos)) }
        Log.v(TAG, "saved todos")
    }

    override suspend fun getTodos(): Result<List<TodoEntity>> {
        val token = TypeToken.getParameterized(ArrayList::class.java, TodoEntity::class.java)
        return try {
            val parsed = gson.fromJson<List<TodoEntity>>(prefs.getString(KEY, ""), token.type)
            parsed.asSuccess()
        } catch (e: JsonParseException) {
            e.asFailure()
        }
    }

    companion object {
        private const val KEY = "Todos"
    }
}