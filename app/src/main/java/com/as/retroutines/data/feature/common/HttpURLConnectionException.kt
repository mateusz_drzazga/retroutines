package com.`as`.retroutines.data.feature.common

import retrofit2.HttpException
import retrofit2.Response
import java.net.HttpURLConnection
import java.net.UnknownHostException

fun <T> createResponseFailure(response: Response<*>): Result<T> {
    return if (response.code() == HttpURLConnection.HTTP_UNAUTHORIZED) {
        UnauthorizedException(response).asFailure()
    } else {
        HttpException(response).asFailure()
    }
}

inline fun <T> catchNetworkException(onException: (e: Exception) -> Result<T> = { throw it }, block: () -> Result<T>): Result<T> {
    return try {
        block()
    } catch (e: Exception) {
        if (e is UnknownHostException) {
            e.asFailure()
        } else onException(e)
    }
}

