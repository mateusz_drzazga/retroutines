package com.`as`.retroutines.data.feature.users

import com.`as`.retroutines.data.feature.common.Result
import com.`as`.retroutines.data.feature.common.asFailure
import com.`as`.retroutines.data.feature.common.map
import com.`as`.retroutines.data.feature.common.onFailureRecover
import com.`as`.retroutines.data.feature.users.local.UserCache
import com.`as`.retroutines.data.feature.users.local.UserEntity
import com.`as`.retroutines.domain.feature.users.User
import com.`as`.retroutines.domain.feature.users.UserDataSource
import java.net.UnknownHostException
import javax.inject.Inject

class UserRepository @Inject constructor(private val store: UserStore, private val cache: UserCache) :
    UserDataSource {

    override suspend fun getUser(id: Int): Result<User> {
        return store.getUser(id).map(UserEntity::toUser)
            .onFailureRecover {
                when (it) {
                    is UnknownHostException -> getUserFromCache(id)
                    else -> it.asFailure()
                }
            }
    }

    private suspend fun getUserFromCache(id: Int): Result<User> {
        return cache.getUser(id).map { it.toUser() }
    }

    override suspend fun deleteUser(user: User): Result<Boolean> {
        return store.deleteUser(user.toEntity())
    }
}