package com.`as`.retroutines.data.feature.authentication.remote

data class Credentials(val email: Email, val password: Password)

inline class Email(val email: String)

inline class Password(val password: String)

// TESTS
val INVALID_CREDENTIALS = Credentials(Email("Mateusz"), Password("Drzazga"))

val VALID_CREDENTIALS = Credentials(Email("eve.holt@reqres.in"), Password("cityslicka"))