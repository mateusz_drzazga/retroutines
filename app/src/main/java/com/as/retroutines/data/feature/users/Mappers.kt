package com.`as`.retroutines.data.feature.users

import com.`as`.retroutines.data.feature.users.local.UserEntity
import com.`as`.retroutines.data.feature.users.remote.UserEntry
import com.`as`.retroutines.domain.feature.users.User

fun UserEntry.toEntity(): UserEntity = UserEntity(id, email, first_name, last_name, avatar)

fun UserEntity.toUser(): User =
    User(id, email, firstName, lastName, avatar)

fun User.toEntity(): UserEntity = UserEntity(id, email, firstName, lastName, avatar)