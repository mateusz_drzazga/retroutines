package com.`as`.retroutines.data.feature.authentication.remote

data class Token(val token: String)