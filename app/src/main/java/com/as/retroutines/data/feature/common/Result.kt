package com.`as`.retroutines.data.feature.common

sealed class Result<out T> {
    data class Success<out T>(val value: T) : Result<T>()
    data class Failure<out T>(val t: Throwable) : Result<T>()
}

fun <T> T.asSuccess(): Result<T> = Result.Success(this)
fun <T> Throwable.asFailure(): Result<T> = Result.Failure(this)


inline fun <R> wrapCatching(block: () -> R): Result<R> {
    return try {
        Result.Success(block())
    } catch (e: Throwable) {
        Result.Failure(e)
    }
}

inline fun <T, R> Result<T>.map(transform: (value: T) -> R): Result<R> = when (this) {
    is Result.Success -> Result.Success(transform(value))
    is Result.Failure -> Result.Failure(t)
}

inline fun <T, R> Result<T>.mapCatching(transform: (value: T) -> R): Result<R> = when (this) {
    is Result.Success -> wrapCatching { transform(value) }
    is Result.Failure -> Result.Failure(t)
}

@JvmName("mapCollectionCatching")
inline fun <T, R> Result<List<T>>.mapCatching(transform: (value: T) -> R): Result<List<R>> = when (this) {
    is Result.Success -> wrapCatching { value.map { transform(it) } }
    is Result.Failure -> Result.Failure(t)
}

inline fun <R, T : R> Result<T>.onFailureRecover(transform: (exception: Throwable) -> Result<R>): Result<R> {
    return when (this) {
        is Result.Success -> this
        is Result.Failure -> transform(this.t)
    }
}

