package com.`as`.retroutines.data.feature.common

import retrofit2.HttpException
import retrofit2.Response

class UnauthorizedException(response: Response<*>) : HttpException(response)

class FormHttpException(response: Response<*>) : HttpException(response) {
    val errorBodyMessage: String? = response.errorBody()?.string()

    override fun toString(): String {
        return "${super.toString()}: $errorBodyMessage"
    }
}