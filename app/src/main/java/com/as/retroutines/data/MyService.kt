package com.`as`.retroutines.data

import com.`as`.retroutines.data.feature.authentication.remote.Credentials
import com.`as`.retroutines.data.feature.authentication.remote.Token
import com.`as`.retroutines.data.feature.todo.remote.TodoEntry
import com.`as`.retroutines.data.feature.users.remote.UserResponse
import com.`as`.retroutines.domain.feature.todo.Todo
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.Response
import retrofit2.http.*

interface MyService {
    @GET("todos/")
    fun getTodosAsynchronously(): Call<List<Todo>>

    @GET("todos/")
    suspend fun getTodosInCoroutine(): Response<List<TodoEntry>>

    @GET("users/{id}")
    suspend fun getUser(@Path("id") id: Int): Response<UserResponse>

    @DELETE("users/{id}")
    suspend fun deleteUser(@Path("id") id: Int): Response<ResponseBody>

    @POST("login")
    suspend fun authenticate(@Body credentials: Credentials): Response<Token>
}