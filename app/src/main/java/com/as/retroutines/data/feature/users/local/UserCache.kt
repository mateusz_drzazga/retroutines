package com.`as`.retroutines.data.feature.users.local

import com.`as`.retroutines.data.feature.users.UserStore

interface UserCache : UserStore {

    suspend fun saveUser(user: UserEntity)
}