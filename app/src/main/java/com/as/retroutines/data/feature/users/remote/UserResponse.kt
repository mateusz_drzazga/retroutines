package com.`as`.retroutines.data.feature.users.remote

data class UserResponse(val data: UserEntry)

data class UserEntry(val id: Int, val email: String, val first_name: String, val last_name: String, val avatar: String)
