package com.`as`.retroutines.data.feature.users.local

import com.`as`.retroutines.data.feature.common.Result
import com.`as`.retroutines.data.feature.common.asFailure
import com.`as`.retroutines.data.feature.common.asSuccess
import com.`as`.retroutines.domain.feature.users.UserNotFoundException
import javax.inject.Inject

class UserRoomCache @Inject constructor(private val dao: UserDao) : UserCache {

    override suspend fun saveUser(user: UserEntity) {
        dao.saveUsers(user)
    }

    override suspend fun getUser(id: Int): Result<UserEntity> {
        return dao.getUserById(id)?.asSuccess() ?: UserNotFoundException(id).asFailure()
    }

    override suspend fun deleteUser(user: UserEntity): Result<Boolean> {
        val deletedCount = dao.deleteUser(user)
        return if (deletedCount == 1) true.asSuccess() else false.asSuccess()
    }
}