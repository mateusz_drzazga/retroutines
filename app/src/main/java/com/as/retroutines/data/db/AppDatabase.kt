package com.`as`.retroutines.data.db

import androidx.room.Database
import androidx.room.RoomDatabase
import com.`as`.retroutines.data.feature.users.local.UserDao
import com.`as`.retroutines.data.feature.users.local.UserEntity

@Database(entities = [UserEntity::class], version = 1)
abstract class AppDatabase : RoomDatabase() {

    abstract fun userDao(): UserDao
}