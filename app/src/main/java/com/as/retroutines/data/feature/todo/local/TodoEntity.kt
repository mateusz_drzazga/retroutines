package com.`as`.retroutines.data.feature.todo.local

data class TodoEntity(val userId: Int, val id: Int, val title: String, val completed: Boolean)