package com.`as`.retroutines.data.feature.todo.local

import com.`as`.retroutines.data.feature.todo.TodoStore

interface TodoCache : TodoStore {

    suspend fun saveTodos(todos: List<TodoEntity>)
}