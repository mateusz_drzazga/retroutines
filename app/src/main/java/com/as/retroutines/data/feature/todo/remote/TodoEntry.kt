package com.`as`.retroutines.data.feature.todo.remote

data class TodoEntry(val userId: Int, val id: Int, val title: String, val completed: Boolean)