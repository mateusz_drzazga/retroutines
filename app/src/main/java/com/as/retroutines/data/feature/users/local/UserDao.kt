package com.`as`.retroutines.data.feature.users.local

import androidx.room.*

@Dao
interface UserDao {

    @Query("SELECT * FROM users WHERE id = :id")
    suspend fun getUserById(id: Int): UserEntity?

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun saveUsers(vararg users: UserEntity): List<Long>

    @Delete
    suspend fun deleteUser(user: UserEntity): Int
}