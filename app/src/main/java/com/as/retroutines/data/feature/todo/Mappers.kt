package com.`as`.retroutines.data.feature.todo

import com.`as`.retroutines.data.feature.todo.local.TodoEntity
import com.`as`.retroutines.data.feature.todo.remote.TodoEntry
import com.`as`.retroutines.domain.feature.todo.Todo

fun TodoEntry?.toEntity(): TodoEntity? {
    return this?.run { TodoEntity(userId, id, title, completed) }
}

fun TodoEntity.toTodo(): Todo {
    return Todo(userId, id, title, completed)
}