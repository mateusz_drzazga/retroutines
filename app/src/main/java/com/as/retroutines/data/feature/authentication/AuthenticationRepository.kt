package com.`as`.retroutines.data.feature.authentication

import com.`as`.retroutines.data.feature.authentication.remote.Credentials
import com.`as`.retroutines.data.feature.authentication.remote.Token
import com.`as`.retroutines.data.feature.common.Result
import com.`as`.retroutines.domain.feature.authentication.AuthenticationDataSource
import javax.inject.Inject

class AuthenticationRepository @Inject constructor(private val store: AuthenticationStore) :
    AuthenticationDataSource {

    override suspend fun authenticate(credentials: Credentials): Result<Token> {
        return store.authenticate(credentials)
    }
}