package com.`as`.retroutines.data.feature.todo

import android.util.Log
import com.`as`.retroutines.data.feature.common.Result
import com.`as`.retroutines.data.feature.common.mapCatching
import com.`as`.retroutines.data.feature.common.onFailureRecover
import com.`as`.retroutines.data.feature.todo.local.TodoCache
import com.`as`.retroutines.data.feature.todo.local.TodoEntity
import com.`as`.retroutines.domain.feature.todo.Todo
import com.`as`.retroutines.domain.feature.todo.TodoDataSource
import javax.inject.Inject

class TodoRepository @Inject constructor(private val store: TodoStore, private val cache: TodoCache) :
    TodoDataSource {
    private val TAG = "TodoRepository"
    override suspend fun getTodos(): Result<List<Todo>> {
        return store.getTodos()
            .mapCatching(TodoEntity::toTodo)
            .onFailureRecover {
                Log.v(TAG, "Fetching todos from network failed. Trying to fetch from cache...")
                cache.getTodos().mapCatching(TodoEntity::toTodo)
            }
    }
}