package com.`as`.retroutines.di

import com.`as`.retroutines.data.feature.authentication.AuthenticationRepository
import com.`as`.retroutines.data.feature.authentication.AuthenticationStore
import com.`as`.retroutines.data.feature.authentication.remote.AuthenticationRemoteStore
import com.`as`.retroutines.domain.feature.todo.TodoDataSource
import com.`as`.retroutines.data.feature.todo.TodoRepository
import com.`as`.retroutines.data.feature.todo.TodoStore
import com.`as`.retroutines.data.feature.todo.local.TodoCache
import com.`as`.retroutines.data.feature.todo.local.TodoRoomCache
import com.`as`.retroutines.data.feature.todo.remote.TodoRemoteStore
import com.`as`.retroutines.domain.feature.users.UserDataSource
import com.`as`.retroutines.data.feature.users.UserRepository
import com.`as`.retroutines.data.feature.users.UserStore
import com.`as`.retroutines.data.feature.users.local.UserCache
import com.`as`.retroutines.data.feature.users.local.UserRoomCache
import com.`as`.retroutines.data.feature.users.remote.UserRemoteStore
import com.`as`.retroutines.domain.feature.authentication.AuthenticationDataSource
import dagger.Binds
import dagger.Module

@Module
interface DataModule {

    @Binds
    fun bindTodoRepository(repository: TodoRepository): TodoDataSource

    @Binds
    fun bindTodoCache(cache: TodoRoomCache): TodoCache

    @Binds
    fun bindTodoStore(store: TodoRemoteStore): TodoStore

    @Binds
    fun bindUserStore(store: UserRemoteStore): UserStore

    @Binds
    fun bindUserCache(cache: UserRoomCache): UserCache

    @Binds
    fun bindUserRepository(repository: UserRepository): UserDataSource

    @Binds
    fun bindAuthenticationStore(store: AuthenticationRemoteStore): AuthenticationStore

    @Binds
    fun bindAuthenticationRepository(repository: AuthenticationRepository): AuthenticationDataSource
}