package com.`as`.retroutines.di

import android.content.SharedPreferences
import android.preference.PreferenceManager
import androidx.room.Room
import com.`as`.retroutines.App
import com.`as`.retroutines.data.MyService
import com.`as`.retroutines.data.db.AppDatabase
import com.`as`.retroutines.data.feature.users.local.UserDao
import com.`as`.retroutines.feature.main.MainActivity
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import dagger.Module
import dagger.Provides
import dagger.android.ContributesAndroidInjector
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Singleton

@Module
abstract class AppModule {

    @ContributesAndroidInjector
    abstract fun mainActivity(): MainActivity

    @Module
    companion object {

        @JvmStatic
        @Provides
        @Singleton
        fun provideGson(): Gson {
            return GsonBuilder().create()
        }

        @JvmStatic
        @Provides
        @Singleton
        fun provideOkHttpClientLoggingInterceptor(): HttpLoggingInterceptor {
            return HttpLoggingInterceptor().apply {
                level = HttpLoggingInterceptor.Level.BODY
            }
        }

        @JvmStatic
        @Provides
        @Singleton
        fun provideOkHttpClient(interceptor: HttpLoggingInterceptor): OkHttpClient {
            return OkHttpClient.Builder()
                .addInterceptor(interceptor)
                .build()
        }


        @JvmStatic
        @Provides
        @Singleton
        fun provideRetrofit(okHttpClient: OkHttpClient, gson: Gson): Retrofit {
            return Retrofit.Builder()
                .client(okHttpClient)
                .baseUrl("https://reqres.in/api/")
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build()
        }

        @JvmStatic
        @Provides
        @Singleton
        fun provideService(retrofit: Retrofit): MyService {
            return retrofit.create(MyService::class.java)
        }

        @JvmStatic
        @Provides
        @Singleton
        fun provideSharedPreferences(app: App): SharedPreferences = PreferenceManager.getDefaultSharedPreferences(app)

        @JvmStatic
        @Provides
        @Singleton
        fun provideDatabase(app: App): AppDatabase =
            Room.databaseBuilder(app, AppDatabase::class.java, "database")
                .fallbackToDestructiveMigration()
                .build()


        @JvmStatic
        @Provides
        @Singleton
        fun profiveUserDao(db: AppDatabase): UserDao = db.userDao()
    }
}