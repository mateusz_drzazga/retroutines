package com.`as`.retroutines

import com.`as`.retroutines.di.DaggerAppComponent
import dagger.android.AndroidInjector
import dagger.android.support.DaggerApplication
import kotlinx.coroutines.CoroutineExceptionHandler

class App : DaggerApplication() {

    override fun onCreate() {
        super.onCreate()
        CoroutineExceptionHandler { coroutineContext, throwable ->  throwable.printStackTrace()}
    }

    override fun applicationInjector(): AndroidInjector<out DaggerApplication>? {
        return DaggerAppComponent.factory().create(this)
    }
}
