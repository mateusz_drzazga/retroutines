package com.`as`.retroutines

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.CoroutineExceptionHandler
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.launch

abstract class BaseViewModel : ViewModel() {
    protected val TAG = this.javaClass.simpleName

    protected val _showProgressLiveData = MutableLiveData<Boolean>()
    val showProgressLiveData: LiveData<Boolean>
        get() = _showProgressLiveData

    protected val errorHandler = CoroutineExceptionHandler { coroutineContext, throwable ->
        Log.v(TAG, "Default error handler")
        throwable.printStackTrace()
        // Report exception
        // Timber.e(throwable)
    }

    /**
     * Helper function that launches new coroutine with CoroutineExceptionHandler and updates showProgress LiveData.
     */
    protected fun launchCoroutine(block: suspend CoroutineScope.() -> Unit) = viewModelScope.launch(errorHandler) {
        _showProgressLiveData.value = true
        block()
        _showProgressLiveData.value = false
    }
}