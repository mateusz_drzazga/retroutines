package com.`as`.retroutines.feature.main

import android.os.Bundle
import android.util.Log
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.`as`.retroutines.BaseActivity
import com.`as`.retroutines.data.MyService
import com.`as`.retroutines.R
import javax.inject.Inject

class MainActivity : BaseActivity() {
    private val TAG = "MainActivity"

    @Inject
    lateinit var service: MyService

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val viewModel = ViewModelProviders.of(this, factory).get(MainViewModel::class.java)
        viewModel.showProgressLiveData.observe(this, Observer {
            Log.v(TAG, "In progress: $it")
        })

//        service.getTodosAsynchronously().enqueue(object : Callback<List<Todo>> {
//            override fun onFailure(call: Call<List<Todo>>, t: Throwable) {
//                Log.v(TAG, "Failure $t")
//            }
//
//            override fun onResponse(call: Call<List<Todo>>, response: Response<List<Todo>>) {
//                Log.v(TAG, "Success ${response.body()}")
//            }
//        })
    }
}
