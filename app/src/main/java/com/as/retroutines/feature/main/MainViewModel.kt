package com.`as`.retroutines.feature.main

import android.util.Log
import androidx.lifecycle.viewModelScope
import com.`as`.retroutines.BaseViewModel
import com.`as`.retroutines.data.feature.authentication.remote.VALID_CREDENTIALS
import com.`as`.retroutines.data.feature.common.FormHttpException
import com.`as`.retroutines.data.feature.common.Result
import com.`as`.retroutines.domain.feature.authentication.AuthenticationDataSource
import com.`as`.retroutines.domain.feature.todo.TodoDataSource
import com.`as`.retroutines.domain.feature.users.User
import com.`as`.retroutines.domain.feature.users.UserDataSource
import kotlinx.coroutines.CoroutineStart
import kotlinx.coroutines.launch
import java.net.UnknownHostException
import javax.inject.Inject

class MainViewModel @Inject constructor(
    private val repository: TodoDataSource,
    private val userRepository: UserDataSource,
    private val authenticationRepository: AuthenticationDataSource
) : BaseViewModel() {

    val userDeferred = viewModelScope.launch(errorHandler, start = CoroutineStart.LAZY) {
        val result = userRepository.getUser(2) // Use id = 23 to fail
        Log.v(TAG, "Fetching user: $result")
    }

    val authenticationJob = viewModelScope.launch(errorHandler) {
        when (val result = authenticationRepository.authenticate(VALID_CREDENTIALS)) {
            is Result.Success -> Log.v(TAG, "Authentication: $result")
            is Result.Failure -> when (val t = result.t) {
                is FormHttpException -> Log.v(TAG, "Form error: $t")
                is UnknownHostException -> Log.v(TAG, "You must be online to sign in!: $t")
                else -> Log.v(TAG, "$t")
            }
        }
    }

    val deleteUserJob = viewModelScope.launch(errorHandler) {
        userDeferred.join()
        val result = userRepository.deleteUser(User(2, "", "", "", ""))
        Log.v(TAG, "Deleting user: $result")
    }
}