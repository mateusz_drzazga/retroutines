Example app using coroutines to handle network requests and database operations.

Data is wrapped in Result.Success class and propagated downstream.

Exceptions are wrapped in Result.Failure class to make handling them easier.